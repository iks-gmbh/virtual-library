# Virtual Library
_Author: Christoph Landsky @ IKS-GmbH 2019_

Dies ist ein Beispielprojekt für eine Kotlin-Fullstack Anwendung, basierend auf Kotlin2JS, VertX und MongoDB. 
Hier kann nach einem Buch per ISBN gesucht werden. Wenn dieses bei Google-Books gefunden wurde, wird das Buch an das Backend weitergegeben und dort gespeichtert. Alle gespeicherten Bücher kann man dann in seinem Bookshelf sehen.
Zu diesem Projekt existiert ein kurzer Einführungsartikel über Kotlin im Fullstack-Betrieb. Siehe [IKS-Blog](http://www.iks-gmbh.com/blog).

Folgendes wird benötigt:
- gradle 6.1
- Java in einer gradle-Kompatiblen Version (z.b. 1.8.221)
- node.js / npm

Für die Datenbank entweder:
- docker / docker-compose
oder eine installierte Version mit einem User "root" und dem Passwort "root", welcher in der admin-Table hinterlegt wurde. Als Datenbank muss dort nur noch `vertxdb` hinterelegt werden.

## Komponenten
### Backend
Das Backend wurde mit VertX realisiert. Dies ist ein leichtgewichtiger, reaktiver Webserver. Man benötigt hierfür keinen Tomcat oder Jetty. Als Datenbank kommt eine MongoDB zum Einsatz. 

### Frontend
Das Frontend nutzt Kotlin-React zur Erstellung einer Singlepage-Application.
Das Styling kommt von: https://codepen.io/marlenesco/pen/NqOozj

!!! Wichtig. Projekt in IntelliJ **öffnen** und nicht importieren. Das .idea Verzeichnis darf nicht überschrieben werden.
Wenn dies doch überschrieben wurde, finden sich, die für den build benötigten _libraries_ im GitLab unter `virtual-library-client/.idea/libraries/`

### App
Dies ist eine simple Android-App, welche einen Barcode einliest und diesen dann an das Backend übermittelt.

## Starten der Beispielanwendung
### mongo-db
```
cd mongo-db/
docker-compose up -d
```
### Backend
``` 
gradle run 
```

### Frontend
```
npm install
npm run start
```
Browser: localhost:3000/
Das Suchfeld nutzt die Google-Books-Api. Gib einfach eine gültige ISBN ein. 
Ein Beispiel wäre 9788711375136. (Eragon)
