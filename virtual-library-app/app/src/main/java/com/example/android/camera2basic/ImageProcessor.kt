package com.example.android.camera2basic

import android.content.Context
import android.graphics.BitmapFactory
import android.media.Image
import android.util.Log
import android.widget.Toast
import com.google.android.gms.vision.Frame
import com.google.android.gms.vision.barcode.Barcode
import com.google.android.gms.vision.barcode.BarcodeDetector

/**
 * Dies ist die von mir angepasste Klasse zum processing des gemachten Fotos... :)
 */
internal class ImageProcessor(
        private val image: Image, //JPEG
        private val context: Context
) : Runnable {

    override fun run() {

        // Einlesen des Bildes
        val buffer = image.planes[0].buffer
        val bytes = ByteArray(buffer.remaining())
        buffer.get(bytes)

        // Barcode Erkennung über die Google Vision API
        var barcodeDetector = BarcodeDetector.Builder(context).build()
        if (!barcodeDetector.isOperational()) {
            println("Could not set up Detector")
            return;
        }
        var bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.size)
        var frame = Frame.Builder().setBitmap(bitmap).build()

        var barcodes = barcodeDetector.detect(frame)

        var thisCode = Barcode()
        if (barcodes.size() > 0) thisCode = barcodes.valueAt(0)

        Toast.makeText(context, thisCode.rawValue, Toast.LENGTH_LONG).show()

        image.close()

        // Senden der ISBN Nummer an das Backend
        try {
            // TODO Parametrized BackendCall ;)
            khttp.post(url = "http://192.168.5.44:8080/books/isbn", json = mapOf("isbn" to thisCode.rawValue))
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    companion object {
        /**
         * Tag for the [Log].
         */
        private val TAG = "ImageProcessor"
    }
}
