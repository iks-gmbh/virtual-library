import io.vertx.core.Vertx

/**
 * Dies ist die Haupt-Runtime. Hier wird lediglich das VertX gestartet
 */
fun main(args: Array<String>) {
    var vertx = Vertx.vertx()
    vertx.deployVerticle("WebVertX")
}