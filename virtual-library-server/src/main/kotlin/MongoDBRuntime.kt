import io.vertx.core.Vertx
import io.vertx.core.json.JsonObject
import io.vertx.ext.mongo.MongoClient

fun main(args: Array<String>) {
    var vertx = Vertx.vertx()
    var mongoConfig = JsonObject()

    mongoConfig.put("db_name", "vertxdb")
            .put("useObjectId", true)
            .put("connection_string", "mongodb://localhost:27017")
            .put("host", "localhost")
            .put("port", 27017)
            .put("username", "root")
            .put("password", "root")
            .put("authSource", "admin")

    var mongoClient = MongoClient.createShared(vertx, mongoConfig)
    println("Ding!")
    val document = JsonObject()
            .put("_id", "5baa1b7810635a2abc716b96")
    mongoClient.findOneAndDelete("books", document) { res ->
        if (res.succeeded()) {
            val id = res.result()
            println("$id")
        } else {
            res.cause().printStackTrace()
        }
    }
    Thread.sleep(5000)

}