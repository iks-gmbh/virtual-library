import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import io.vertx.core.AbstractVerticle
import io.vertx.core.Future
import io.vertx.core.json.JsonObject
import io.vertx.ext.mongo.MongoClient
import io.vertx.ext.web.Router
import io.vertx.ext.web.RoutingContext
import io.vertx.ext.web.client.WebClient
import io.vertx.ext.web.handler.BodyHandler
import io.vertx.ext.web.handler.CorsHandler
import io.vertx.ext.web.handler.StaticHandler
import io.vertx.kotlin.core.json.get
import model.GoogleBooksResponseMap


data class JSONContent(var name: String, var value: String) {
    fun toJSON(): String {
        return jacksonObjectMapper().writeValueAsString(this)
    }
}

/**
 * Die Klasse WebVertX ist die Implementierung des AbstractVerticles udn stellt einen WebServer mit
 * entsprechenden Routen zur Verfügung
 */
class WebVertX : AbstractVerticle() {

    private var mongoClient: MongoClient? = null

    override fun start(future: Future<Void>) {
        // Initialisieren der MongoDB
        var mongoConfig = JsonObject()
        mongoConfig.put("db_name", "vertxdb")
                .put("useObjectId", true)
                .put("connection_string", "mongodb://localhost:27017")
                .put("host", "127.0.0.1")
                .put("port", 27017)
                .put("username", "root")
                .put("password", "root")
                .put("authSource", "admin")

        var mongoClient = MongoClient.createShared(vertx, mongoConfig)
        var router = Router.router(vertx)

        this.mongoClient = mongoClient

        // Erstellen der Routen
        this.createRoutes(router)

        // HttpServer erstellen
        vertx.createHttpServer()
                .requestHandler(router::accept)
                .listen(config().getInteger("http.port", 8080)) { result ->
                    if (result.succeeded()) future.complete()
                    else future.fail(result.cause())
                }
    }

    private fun createRoutes(router: Router) {
        // Konfiguration des Routers (CORS, Allowed Methods)
        router.route().handler(
                CorsHandler.create(".*")
                        .allowedMethod(io.vertx.core.http.HttpMethod.GET)
                        .allowedMethod(io.vertx.core.http.HttpMethod.POST)
                        .allowedMethod(io.vertx.core.http.HttpMethod.OPTIONS)
                        .allowedHeader("Access-Control-Allow-Method")
                        .allowedHeader("Access-Control-Allow-Origin")
                        .allowedHeader("Content-Type")
        )

        // Erstellung statischer Routen
        router.route("/client/build/*").handler(StaticHandler.create("client")
                .setDirectoryListing(true))

        router.route("/static*").handler(StaticHandler.create("../../client/build/static").setDirectoryListing(true))

        // Serve assets resources from the /assets directory
        router.route("/assets/*").handler(StaticHandler.create("assets")
                .setDirectoryListing(true)
        )

        // Die einzelnen Routen
        router.route("/").handler { routingContext ->
            var response = routingContext.response()
            response.sendFile("../../client/build/index.html")
        }

        // REST-API
        router.route("/api").handler { routingContext ->
            var response = routingContext.response()
            response.putHeader("content-type", "application/json")
                    .end(JSONContent("value", "Hallo!").toJSON())
        }

        router.route("/books*").handler(BodyHandler.create())
        router.get("/books").handler(this::getBooks)
        router.post("/books").handler(this::insertBook)
        router.post("/books/isbn").handler(this::insertBookByIsbn)
    }

    /**
     * Dieser Handler bekommt lediglich eine ISBN Nummer und sammelt sich die Informationen über den Google
     * Web Service selbst zusammen. Die ermittelten Daten werden schließlich in der Datenbank abgelegt.
     * Zum parsen wird Jackson XML verwendet
     */
    private fun insertBookByIsbn(routingContext: RoutingContext) {

        // Werden für das request-handling benötigt.
        var request = routingContext.request()
        var response = routingContext.response()

        var jsonObject = routingContext.bodyAsJson
        val isbn = jsonObject.get<String>("isbn")

        if (isbn != "") {
            var webClient = WebClient.create(vertx)
            webClient
                    .getAbs("https://www.googleapis.com/books/v1/volumes?q=isbn:$isbn&key=AIzaSyDtOH0_s4zCGkYllzw4GEMXQIo-xLtCtvQ&maxResults=1")
                    .send { ar ->
                        // Callback für den HTTP GET Aufruf
                        if (ar.succeeded()) {
                            // Response des Aufrufs
                            val responseOrigin = ar.result()

                            // Jackson XML Mapper (als Kotlin-Mapper)
                            val mapper = ObjectMapper().registerKotlinModule()
                            // Konfiguration, damit das parsen nicht abgebrochen wird, wenn ein unbekanntes
                            // JSON-Property versucht wird zu parsen.
                            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

                            var responseMap = mapper.readValue<GoogleBooksResponseMap>(responseOrigin.bodyAsString())

                            val gbooksId = responseMap.items?.get(0)?.id ?: ""
                            val title = responseMap.items?.get(0)?.volumeInfo?.title ?: ""

                            if (gbooksId != "") {
                                val documentToSave = JsonObject()
                                        .put("gbooks_id", gbooksId)
                                        .put("title", title)
                                        .put("isbn", isbn)

                                mongoClient?.save("books", documentToSave) { res ->
                                    if (res.succeeded()) {
                                        val id = res.result()
                                        println("Saved book with id $id")
                                    } else {
                                        res.cause().printStackTrace()
                                    }
                                }
                            }
                        }
                    }
        }
        response.setStatusCode(200).end()
    }


    /**
     * Holt alle Bücher aus der Datenbank
     */
    private fun getBooks(routingContext: RoutingContext) {
        val query = JsonObject()
        mongoClient?.find("books", query) { res ->
            var response = routingContext.response()
            if (res.succeeded()) {
                var items = mutableListOf<JsonObject>()
                response.putHeader("content-type", "text/json")
                for (json in res.result()) {
                    items.add(json)
                }
                response.end(items.toString())

            } else {
                res.cause().printStackTrace()
            }
        }
    }

    /**
     * Nimmt ein bereits gefülltes JSONObject entgegen und legt es in der Datenbank ab
     */
    private fun insertBook(routingContext: RoutingContext) {
        var request = routingContext.request()
        var response = routingContext.response()


        val title = request.getFormAttribute("title") ?: ""
        val id = request.getFormAttribute("id") ?: ""
        val isbn = request.getFormAttribute("isbn") ?: ""

        val document = JsonObject()
                .put("gbooks_id", id)
                .put("title", title)
                .put("isbn", isbn)

        mongoClient?.save("books", document) { res ->
            if (res.succeeded()) {
                val id = res.result()
                println("Saved book with id $id")
            } else {
                res.cause().printStackTrace()
            }
        }
        response.setStatusCode(201).end()
    }
}