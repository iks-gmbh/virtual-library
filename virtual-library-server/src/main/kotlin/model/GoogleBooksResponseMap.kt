package model

data class GoogleBooksResponseMap(
        val kind: String? = "",
        val totalItems: Int? = 0,
        var items: Array<Item>? = arrayOf()
)


data class Item(
        val kind: String? = "",
        val id: String? = "",
        val volumeInfo: VInfo? = VInfo()
)

data class VInfo(
        val title: String? = "",
        val subtitle: String? = "",
        val authors: Array<String>? = arrayOf(),
        val imageLinks: ImageLinks? = ImageLinks(),
        val industryIdentifiers: Array<IndustryIdentifier>? = arrayOf()
)

data class ImageLinks(
        val smallThumbnail: String? = "",
        val thumbnail: String? = ""
)

data class IndustryIdentifier(
        val type: String? = "",
        val identifier: String? = ""
)