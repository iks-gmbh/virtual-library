package index

import components.app.AddBooksToLibraryApp
import components.app.BookshelfApp
import kotlinext.js.js
import kotlinext.js.require
import kotlinext.js.requireAll
import kotlinx.html.style
import react.RBuilder
import react.RComponent
import react.RProps
import react.RState
import react.dom.*
import react.router.dom.hashRouter
import react.router.dom.redirect
import react.router.dom.route
import react.router.dom.switch
import kotlin.browser.document


/**
 * Die Main Methode wird von der Webseite aufgerufen.
 */
@Suppress("UnsafeCastFromDynamic")
fun main(args: Array<String>) {
    // Einbetten der CSS-Dateien.
    requireAll(require.context("src", true, js("/\\.css$/")))

    // render "holt" sich das <div id="root" /> Element und fügt die Klasse root() (siehe Unten) als Child hinzu
    render(document.getElementById("root")) {
        root()
    }
}

/**
 * React-Properties zur Weitergabe von Informationen von Ausserhalb
 */
interface IdProps : RProps {
    var id: Int
}

/**
 * Die Root Komponente der App. Hier wird u.a. das Routing, sowie der allgmeine Aufbau der Seite vorgenommen
 */
class RootComponent : RComponent<RProps, RState>() {
    @Suppress("UnsafeCastFromDynamic")
    override fun RBuilder.render() {
        nav("navbar navbar-expand-lg navbar-light bg-light sticky-top") {
            ul("navbar-nav mr-auto") {
                li("nav-item") {
                    a("nav-link") {
                        attrs.style = js { marginRight = "20px" }
                        attrs.href = "#/bookshelf"
                        +"Bookshelf"
                    }
                }
                li("nav-item") {
                    a("nav-link") {
                        attrs.style = js { marginRight = "20px" }

                        attrs.href = "#/search"
                        +"Search"
                    }
                }
            }
        }

        // HashRouter -> myseite.de/#/search
        hashRouter {
            // or "browserRouter"
            switch {
                route("/search", AddBooksToLibraryApp::class, exact = true)
                route("/bookshelf", BookshelfApp::class, exact = true)
                redirect(from = "/", to = "/search")
            }
        }
    }
}

/**
 * Mapped die Klasse Root auf die Methode root(). Hier können weitere Initialisierungsaufgaben wie das befüllen der
 * Properties erledigt werden.
 */
fun RBuilder.root() = child(RootComponent::class) {
    //attrs.items = items
}



