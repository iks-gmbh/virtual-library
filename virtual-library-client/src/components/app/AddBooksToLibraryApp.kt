package components.app

import model.GoogleBooksResponseMap
import kotlinext.js.*
import kotlinx.html.*
import kotlinx.html.js.*

import org.w3c.dom.events.Event
import org.w3c.dom.*
import org.w3c.xhr.*
import react.*
import react.dom.*
import model.Item

/**
 * Der RState repräsentiert den Zustand der WebAnwendung (Client) und
 * beinhaltet alle Variablen, die sich zur Laufzeit ändern.
 * Wenn setState {} aufgerufen wird, wird die Seite neu gerendert
 */
interface FetchGoogleState : RState {
    var books: MutableList<Item>
    var namesOfDisabledSaveButtons: MutableList<String>
    var rerender: Int
}

/**
 * Diese Klasse dient dem Hinzufügen eines Buches mithilfe einer API Suche auf der GoogleBooksAPI.
 * Als Suchparameter wird die ISBN Nummer verwendet.
 */
class AddBooksToLibraryApp : RComponent<RProps, FetchGoogleState>() {

    // Initialisierung des RStates
    override fun FetchGoogleState.init() {
        books = mutableListOf()
        namesOfDisabledSaveButtons = mutableListOf()
        rerender = 0
    }

    /**
     * Sendet das gesuchte Buch zum Backend
     */
    fun insertBookToLibrary(item: Item) {
        val data = FormData()
        val id = item.id
        val title = item.volumeInfo?.title
        val isbn = item.volumeInfo?.industryIdentifiers?.get(0)?.identifier

        if (isbn.notNull() && id.notNull()) {
            data.append("title", title ?: "")
            data.append("isbn", isbn ?: "")
            data.append("id", id ?: "")

            val xhttp = XMLHttpRequest()
            xhttp.open("POST", "http://localhost:8080/books", true)
            xhttp.onreadystatechange = {
            }
            xhttp.send(data)
        }
    }

    // Ermöglicht den folgenden Aufruf: myvar.notNull()
    fun Any?.notNull(): Boolean {
        return this != null
    }

    /**
     * Holt anhand der eingegebenen isbn das zugehörige Buch aus der GBooks API
     */
    fun fetchItems(isbn: String, count: String) {
        val xhttp = XMLHttpRequest()
        xhttp.open("GET", "https://www.googleapis.com/books/v1/volumes?q=isbn:$isbn&key=AIzaSyDtOH0_s4zCGkYllzw4GEMXQIo-xLtCtvQ&orderBy=relevance&maxResults=${count}", true)
        xhttp.onloadend = fun(e: Event) {
            val response = JSON.parse<GoogleBooksResponseMap>(xhttp.responseText)
            val newBookState = state.books

            // Dieser Block wird nur dann ausgeführt, wenn ein item da ist
            response.items?.first()?.let {
                newBookState.add(it)
                insertBookToLibrary(it)
            }

            setState { books = newBookState }
        }
        xhttp.send()
    }

    /**
     * Event-Handler für den Search Button
     */
    fun submitBookSearch(e: Event) {
        e.preventDefault()
        val form = e.target as HTMLFormElement
        val input = form[0] as HTMLInputElement
        val count = form[1] as HTMLInputElement
        val q = input.value.replace(" ", "+")
        input.value = ""
        if (q != "") {
            fetchItems(q, count.value)
        }

    }

    /**
     * Die render Methode definiert, was später als HTML-Code auf der Seite sichtbar sein wird
     */
    override fun RBuilder.render() {

        val stateVal = state.books

        // div, h1, etc.. sind klassische HTML Tags. Die Strings in der Klammer sind CSS Klassen, in diesem Fall Bootstrap
        div("container") {
            h1 {
                // Zusätzliche Attribute können direkt im attrs Objekt gesetzt werden
                attrs.style = js {
                    marginTop = "10px"; textAlign = "center";
                    verticalAlign = "middle";
                    lineHeight = "90px"
                }

                /**
                 * h1{
                 *  +"Hallo"
                 * }
                 * Entspricht
                 * <h1>Hallo</h1>
                 */

                +"Awesome Books"
            }

            div("card") {
                div("card-body") {
                    h5("card-title") {
                        +"Suchoptionen"
                    }
                    form("form-group") {
                        attrs.onSubmitFunction = this@AddBooksToLibraryApp::submitBookSearch
                        div("row") {
                            div("col-md-5 col-sm-12") {
                                input(classes = "form-control", type = InputType.text) {
                                    attrs.placeholder = "Search for isbn of awesome books here!"
                                }
                            }
                            div("col-md-5 col-sm-12") {
                                input(classes = "form-control", type = InputType.number) {
                                    attrs.defaultValue = "1"
                                    attrs.min = "1"
                                    attrs.max = "10"
                                }
                            }
                            div("col-md-2 col-sm-12") {
                                button(classes = "btn btn-outline my-2 my-sm-0", type = ButtonType.submit) {
                                    +"Search!"
                                }
                            }
                        }

                    }

                }


            }

            div("row active-with-click") {
                attrs.style = js { marginTop = "20px" }

                /**
                 * That is the Power of React/kotlinJS. In diesem Fall werden die HTML-Elemente durch Listeniteration
                 * dynamisch gebaut!
                 */
                stateVal.forEach { item ->
                    var itemvalues = "ID: ${item.id} | Title: ${item.volumeInfo?.title}"
                    div("col-md-4 col-sm-6 col-xs-12 card-holder") {
                        article("material-card Red") {
                            div("mc-content") {
                                div("img-container") {

                                    img(classes = "img-responsive", src = "${item.volumeInfo?.imageLinks?.thumbnail}") {
                                    }
                                }
                            }
                            h2 {
                                div {
                                    +"${item.volumeInfo?.title}"
                                }
                                strong {
                                    i("fa fa-fw fa-star") {

                                    }
                                    var authors = ""
                                    val authorsArray: Array<String>? = item.volumeInfo?.authors
                                    if (authorsArray != null && authorsArray.isNotEmpty()) {
                                        authorsArray.forEach { author ->
                                            authors += "${author} "
                                        }
                                    }
                                    +authors
                                }

                            }

                        }
                    }
                }
            }
        }
    }
}

fun RBuilder.addbook() = child(AddBooksToLibraryApp()::class) {
    //attrs.items = items
}


