package components.app

import kotlinx.html.style
import model.AppState
import model.MyBook
import model.GoogleBooksResponseMap
import org.w3c.dom.events.Event
import org.w3c.xhr.XMLHttpRequest
import react.RBuilder
import react.RComponent
import react.RProps
import react.dom.*
import react.setState

/**
 * Dient zur Anzeige aller bereits eingetragenen Bücher
 */
class BookshelfApp : RComponent<RProps, AppState>() {
    override fun AppState.init() {
        books = mutableListOf()
    }

    fun fetchDataFromServer() {
        val xhttp = XMLHttpRequest()
        xhttp.open("GET", "http://localhost:8080/books", true)
        xhttp.onloadend = fun(e: Event) {
            val response = JSON.parse<Array<MyBook>>(xhttp.responseText)

            response.forEach { item ->
                item.isbn?.let { fetchFromGoogleBooks(it) }
            }

        }
        xhttp.send()
    }

    fun Any?.notNull(): Boolean {
        return this != null
    }

    fun fetchFromGoogleBooks(isbn: String) {
        val xhttp = XMLHttpRequest()
        xhttp.open("GET", "https://www.googleapis.com/books/v1/volumes?q=isbn:$isbn&key=AIzaSyDtOH0_s4zCGkYllzw4GEMXQIo-xLtCtvQ&orderBy=relevance&maxResults=${1}", true)
        xhttp.onloadend = fun(e: Event) {
            val response = JSON.parse<GoogleBooksResponseMap>(xhttp.responseText)
            val newbooks = state.books

            response.items?.first()?.let { newbooks.add(it) }
            setState { books = newbooks }
        }
        xhttp.send()
    }

    override fun componentWillMount() {
        fetchDataFromServer()
    }


    override fun RBuilder.render() {
        val stateVal = state.books

        div("container") {
            h1 {
                attrs.style = kotlinext.js.js {
                    marginTop = "10px"; textAlign = "center"
                    verticalAlign = "middle"
                    lineHeight = "90px"
                }
                +"These are your books"
            }
            div("row active-with-click") {
                attrs.style = kotlinext.js.js { marginTop = "20px" }

                stateVal.forEach { item ->
                    var itemvalues = "ID: ${item.id} | Title: ${item.volumeInfo?.title}"
                    div("col-md-4 col-sm-6 col-xs-12 card-holder") {
                        article("material-card Red") {
                            div("mc-content") {
                                div("img-container") {

                                    img(classes = "img-responsive", src = "${item.volumeInfo?.imageLinks?.thumbnail}") {
                                    }
                                }
                            }
                            h2 {
                                div {
                                    +"${item.volumeInfo?.title}"
                                }
                                strong {
                                    i("fa fa-fw fa-star") {

                                    }
                                    var authors = ""
                                    val authorsArray: Array<String>? = item.volumeInfo?.authors
                                    if (authorsArray != null && authorsArray.isNotEmpty()) {
                                        authorsArray.forEach { author ->
                                            authors += "$author "
                                        }
                                    }
                                    +authors
                                }
                            }

                        }
                    }
                }
            }
        }
    }
}

fun RBuilder.mybooks() = child(BookshelfApp()::class) {
    //attrs.items = items
}