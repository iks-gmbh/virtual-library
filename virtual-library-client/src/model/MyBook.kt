package model

/**
 * Dient zum Mappen der Response vom Backend.
 */
data class MyBook(var isbn: String? = "", var gbooks_id: String? = "", var title: String? = "", var id: String? = "")
