package model

import react.RState

interface AppState : RState {
    var books: MutableList<Item>
}